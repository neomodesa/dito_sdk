import 'dart:async';

import 'package:dito_sdk/src/http/http_service.dart';
import 'package:dito_sdk/src/model/track_model.dart';

import 'model/dito_sdk_error.dart';
import 'model/identify_model.dart';

class DitoSdk {
  final String appKey, appSecret;
  HttpService _dio;
  DitoSdk({this.appKey, this.appSecret}) {
    _dio = HttpService(appKey: appKey, appSecret: appSecret);
  }

  Future<Map<String, dynamic>> setIdentify(int id, IdentifyModel model, String token) async {
    // NOTE os campos tem que ser obrigatorios

    if (id == null) {
      throw DitoSdkError('Invalid User ID');
    }

    if (model.name == null) {
      throw DitoSdkError('Invalid user name');
    }

    if (model.email == null) {
      throw DitoSdkError('Invalid user email');
    }

    var resp = await _dio.identify(id, data: model);

    if (resp.hasError) {
      throw DitoSdkError(resp.error.message);
    }

    var respToken = await _dio.registerTokenMobile(id, token);

    if (respToken.hasError) {
      throw DitoSdkError(respToken.error?.message);
    }

    return resp.data;
  }

  Future<Map<String, dynamic>> setTrack(int id, TrackModel model) async {
    var resp = await _dio.track(id, data: model);

    if (resp.hasError) {
      throw DitoSdkError(resp.error.message);
    }
    return resp.data;
  }

  Future<Map<String, dynamic>> disabledTokenMobile(int id, String token) async {
    var resp = await _dio.disabledTokenMobile(id, token);

    if (resp.hasError) {
      throw DitoSdkError(resp.error.message);
    }
    return resp.data;
  }

  bool get hasDitoConfigured => (appKey != null && appSecret != null);
}
