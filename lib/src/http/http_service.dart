import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:dito_sdk/dito_sdk.dart';
import 'package:dito_sdk/src/model/dito_sdk_error.dart';
import 'package:dito_sdk/src/model/dito_sdk_response.dart';

import 'interceptores/log_interceptor.dart' as _log;

class HttpService {
  final String appKey;
  final _dio = Dio();
  String sha1Signature;

  HttpService({this.appKey, String appSecret}) {
    /// options.baseUrl = FlavorConfig.instance.urlApi;
    sha1Signature = _sha1(appSecret);
    _dio.interceptors.add(_log.LogInterceptor());
  }

  Future<DitoSdkResponse> identify(int id, {IdentifyModel data}) async {
    if (id == null) {
      return DitoSdkResponse(
          hasError: true, error: DitoSdkError('Id do usuario invalido'));
    }
    var model = {
      'platform_api_key': appKey,
      'sha1_signature': sha1Signature,
      'user_data': data.toJson()
    };

    var resp = await _dio.post(
        'https://login.plataformasocial.com.br/users/portal/$id/signup',
        data: model);

    // check Error
    var error = _hasError(resp.data);

    if (error != null) {
      return DitoSdkResponse(data: resp.data, hasError: true, error: error);
    }

    return DitoSdkResponse(data: resp.data);
    //  resp.data;
  }

  Future<DitoSdkResponse> track(int id, {TrackModel data}) async {
    if (id == null) {
      return DitoSdkResponse(
          hasError: true,
          error: DitoSdkError(
              'Precisa indentificar o dispositivo antes de fazer um evento'));
    }

    if (data == null) {
      return DitoSdkResponse(
          hasError: true, error: DitoSdkError('Dados invalidos'));
    }
    var model = {
      'platform_api_key': appKey,
      'sha1_signature': sha1Signature,
      'id_type': 'id',
      'network_name': 'pt',
      'event': json.encode({
        'action': data.action,
        'created_at': data.createdAt,
        'revenue': data.revenue,
        'data': (data == null) ? null : data.data,
      }),
    };
    var resp = await _dio
        .post('https://events.plataformasocial.com.br/users/$id', data: model);

    // check Error
    var error = _hasError(resp.data);

    if (error != null) {
      return DitoSdkResponse(data: resp.data, hasError: true, error: error);
    }
    return DitoSdkResponse(data: resp.data);
    //  resp.data;
  }

  Future<DitoSdkResponse> registerTokenMobile(int id, String token) async {
    if (id == null) {
      return DitoSdkResponse(
          hasError: true, error: DitoSdkError('Id do usuario invalido'));
    }

    var model = {
      'id_type': 'id',
      'platform_api_key': appKey,
      'sha1_signature': sha1Signature,
      'platform': (Platform.isIOS) ? 'Apple iPhone' : 'Android',
      'token': token,
    };

    Response<dynamic> resp;

    try {
      resp = await _dio.post(
          'https://notification.plataformasocial.com.br/users/$id/mobile-tokens/',
          data: model);
    } on DioError catch (e) {
      resp = e.response;
    }

    var error = _hasError(resp.data);

    if (error != null) {
      return DitoSdkResponse(data: resp.data, hasError: true, error: error);
    }
    return DitoSdkResponse(data: resp.data);
  }

  Future<DitoSdkResponse> disabledTokenMobile(int id, String token) async {
    if (id == null) {
      return DitoSdkResponse(
          hasError: true, error: DitoSdkError('Id do usuario invalido'));
    }

    var model = {
      'id_type': 'id',
      'platform_api_key': appKey,
      'sha1_signature': sha1Signature,
      'platform': (Platform.isIOS) ? 'Apple iPhone' : 'Android',
      'token': token,
    };

    var resp = await _dio.post(
        'https://notification.plataformasocial.com.br/users/$id/mobile-tokens/disable/',
        data: model);

    // check Error
    var error = _hasError(resp.data);

    if (error != null) {
      return DitoSdkResponse(data: resp.data, hasError: true, error: error);
    }
    return DitoSdkResponse(data: resp.data);
  }

  // NOTE Funções anonimas

  String _sha1(String value) {
    var bytes = utf8.encode(value); // data being hashed

    return sha1.convert(bytes).toString();
  }

  DitoSdkError _hasError(Map data) {
    if (data != null) {
      if (data.containsKey('error')) {
        return DitoSdkError(data['error']['message']);
      }
    }
    return null;
  }
}
