import 'dart:developer';

import 'package:dio/dio.dart' as dio;

class LogInterceptor extends dio.InterceptorsWrapper {
  DateTime timeStart;
  DateTime timeEnd;

  @override
  Future onRequest(dio.RequestOptions options) async {
    timeStart = DateTime.now();
    log('OnRequest [${timeStart}ms] | [${options?.method}] => ${options?.uri}');

    return super.onRequest(options);
  }

  @override
  Future onResponse(dio.Response response) {
    timeEnd = DateTime.now();

    log('OnResponse [${timeRequest}ms] | [${response?.statusCode}] | LENGTH: ${response?.data?.length}');
    return super.onResponse(response);
  }

  @override
  Future onError(dio.DioError err) {
    timeEnd = DateTime.now();
    log(' OnError [${timeRequest}ms] [${err?.response?.statusCode}] | LENGTH ${err?.response?.data}');

    return super.onError(err);
  }

  get timeRequest => (timeEnd.millisecondsSinceEpoch - timeStart.millisecondsSinceEpoch);
}
