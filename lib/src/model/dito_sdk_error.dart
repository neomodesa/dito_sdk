/// Represents error of uno
class DitoSdkError implements Exception {
  final String message;
  final StackTrace stackTrace;

  const DitoSdkError(this.message, {this.stackTrace});

  @override
  String toString() {
    return 'DitoSdkError: $message\n${stackTrace != null ? '\n\n' + stackTrace.toString() : ''}';
  }
}
