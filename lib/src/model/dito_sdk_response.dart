import 'package:dito_sdk/src/model/dito_sdk_error.dart';

class DitoSdkResponse {
  bool hasError;
  DitoSdkError error;
  Map<String, dynamic> data;

  DitoSdkResponse({this.data, this.hasError = false, this.error});

  @override
  String toString() => (hasError) ? 'ERROR: $hasError, Message:  $error' : '$data';
}
