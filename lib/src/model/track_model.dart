class TrackModel {
  String action;
  double revenue;
  String createdAt;
  Map<String, dynamic> data;

  TrackModel({this.action, this.data, this.revenue, this.createdAt});
}
