import 'package:intl/intl.dart';
import 'dart:convert';

class IdentifyModel {
  String name, email, location;
  genderType gender; // male or female
  DateTime birthday; // yyyy-mm-dd
  DateTime createdAt; // yyyy-mm-dd hh:mm:ss -z
  Map<String, dynamic> data;

  IdentifyModel(
      {this.name,
      this.email,
      this.location,
      this.gender,
      this.birthday,
      this.createdAt,
      this.data});

  get formattedBirthday {
    return birthday == null ? null : DateFormat('yyyy-MM-dd').format(birthday);
  }

  Map<String, dynamic> toJson() => {
        'name': name,
        'email': email,
        'gender': (gender == null) ? null : gender.string,
        'location': location,
        'birthday': formattedBirthday,
        'created_at': createdAt == null
            ? null
            : '${DateFormat('yyyy-MM-dd HH:mm:ss').format(createdAt)}',
        'data': (data == null) ? null : json.encode(data),
      };
}

enum genderType { male, female }

extension genderEx on genderType {
  static const Map<genderType, String> _type = {
    genderType.male: 'male',
    genderType.female: 'female',
  };

  String get string => _type[this];
}
