import 'package:dito_sdk/dito_sdk.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _dito = DitoSdk(
    appKey: 'MjAyMS0wMi0yNiAxNzozNjoxNCAtMDMwMERhc2ggcGFyYSB0ZXN0ZSAyNjczMg',
    appSecret: 'vmIXxE31EpJ5E6aEzy9rqx7goljNkqEGxmodZZFg',
  );
  @override
  void initState() {
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Dito example'),
        ),
        body: Center(
          child: Column(
            children: [
              ElevatedButton(
                onPressed: () async {
                  try {
                    await _dito.setIdentify(
                      08637739903,
                      IdentifyModel(
                        name: 'teste api 1801',
                        email: 'testeapi@teste.com.br',
                        location: 'belo horizonte',
                        gender: genderType.male,
                        birthday: DateTime.parse('1996-05-08'),
                        createdAt: DateTime.parse('2022-02-09 12:03:03'),
                        data: {
                          'data1': 'dado1',
                          'dato2': 'dado2'
                        },
                      ),
                      '123456',
                    );
                    _showSnackbar('Usuario identificado!');
                  } on DitoSdkError catch (e) {
                    _showSnackbar(e.message);
                  }
                },
                child: Text(
                  'Identificar',
                ),
              ),
              ElevatedButton(
                onPressed: () async {
                  try {
                    await _dito.setTrack(
                        08637739903,
                        TrackModel(
                          action: 'disparo-domingo',
                          revenue: null,
                          data: {
                            'extra': '123456'
                          },
                        ));

                    _showSnackbar('Track realizado');
                  } on DitoSdkError catch (e) {
                    _showSnackbar(e.message);
                  }

                  // *********
                },
                child: Text(
                  'Criar track',
                ),
              ),
              //
              ElevatedButton(
                onPressed: () async {
                  try {
                    var teste = await _dito.disabledTokenMobile(08637739903, '123456');

                    _showSnackbar('Disabled token $teste');
                  } on DitoSdkError catch (e) {
                    _showSnackbar(e.message);
                  }

                  // *********
                },
                child: Text('DESABILITAR DISPOSITIVO'),
              ),
              //
            ],
          ),
        ),
      ),
    );
  }

  _showSnackbar(String msg) => _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(msg ?? '')));
}
